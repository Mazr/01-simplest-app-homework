package com.twuc.webApp;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.http.MediaType.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
@AutoConfigureMockMvc
class AdditionTableControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_200_status_given_mock_request() throws Exception {
        mockMvc.perform(get("/api/tables/plus"))
                .andExpect(status().is(200));
    }

    @Test
    void should_return_text_plain_given_mock_request() throws Exception {
        mockMvc.perform(get("/api/tables/plus"))
                    .andExpect(content().contentTypeCompatibleWith("text/plain"));
    }

    @Test
    void should_return_plus_table_as_string() throws Exception {
        String actually =
                "1+1=2\t\r\n" +
                        "2+1=3\t2+2=4\t\r\n" +
                        "3+1=4\t3+2=5\t3+3=6\t\r\n" +
                        "4+1=5\t4+2=6\t4+3=7\t4+4=8\t\r\n" +
                        "5+1=6\t5+2=7\t5+3=8\t5+4=9\t5+5=10\t\r\n" +
                        "6+1=7\t6+2=8\t6+3=9\t6+4=10\t6+5=11\t6+6=12\t\r\n" +
                        "7+1=8\t7+2=9\t7+3=10\t7+4=11\t7+5=12\t7+6=13\t7+7=14\t\r\n" +
                        "8+1=9\t8+2=10\t8+3=11\t8+4=12\t8+5=13\t8+6=14\t8+7=15\t8+8=16\t\r\n" +
                        "9+1=10\t9+2=11\t9+3=12\t9+4=13\t9+5=14\t9+6=15\t9+7=16\t9+8=17\t9+9=18\t\r\n";
        mockMvc.perform(get("/api/tables/plus"))
                .andExpect(content().string(actually));

    }

    @Test
    void should_return_plus_table_with_start_and_end() throws Exception {
        String actually = "3+3=6\t\r\n" +
                "4+3=7\t4+4=8\t\r\n" +
                "5+3=8\t5+4=9\t5+5=10\t\r\n";
        mockMvc.perform(get("/api/tables/plus?start=3&end=5"))
                .andExpect(status().isOk())
                .andExpect(content().string(actually));

    }

    @Test
    void should_return_400_status_with_error_start_and_end() throws Exception {
        mockMvc.perform(get("/api/tables/plus?start=3&end=10"))
                .andExpect(status().is(400));
        mockMvc.perform(get("/api/tables/plus?start=6&end=5"))
                .andExpect(status().is(400));
        mockMvc.perform(get("/api/tables/plus?start=6&end=abc"))
                .andExpect(status().is(400));
    }

    @Test
    void should_return_plus_table_format_to_json() throws Exception {
        String expectedContent = "[" +
                "[\"4+4=8\"]," +
                "[\"5+4=9\",\"5+5=10\"]," +
                "[\"6+4=10\",\"6+5=11\",\"6+6=12\"]" +
                "]";
        mockMvc.perform(get("/api/tables/plus?start=4&end=6").accept(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    void should_return_plus_table_format_to_html() throws Exception {
        String expectedContent = "<ol>\n" +
                "  <li>\n" +
                "    <ol>\n" +
                "      <li>4+4=8</li>\n" +
                "    </ol>\n" +
                "  <li>\n" +
                "  <li>\n" +
                "    <ol>\n" +
                "      <li>5+4=9</li>\n" +
                "      <li>5+5=10</li>\n" +
                "    </ol>\n" +
                "  <li>\n" +
                "  <li>\n" +
                "    <ol>\n" +
                "      <li>6+4=10</li>\n" +
                "      <li>6+5=11</li>\n" +
                "      <li>6+6=12</li>\n" +
                "    </ol>\n" +
                "  <li>\n" +
                "</ol>\n";
        mockMvc.perform(get("/api/tables/plus?start=4&end=6").accept(TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));

    }
}
