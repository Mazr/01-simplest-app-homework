package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.http.MediaType.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class MultiplicationTableControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_200_status_given_mock_request() throws Exception {
        mockMvc.perform(get("/api/tables/multiply"))
                .andExpect(status().is(200));
    }

    @Test
    void should_return_text_plain_given_mock_request() throws Exception {
        mockMvc.perform(get("/api/tables/multiply"))
                .andExpect(content().contentTypeCompatibleWith("text/plain"));
    }

    @Test
    void should_return_mul_table_as_string() throws Exception {
        String actually = "1*1=1\t\r\n" +
                "2*1=2\t2*2=4\t\r\n" +
                "3*1=3\t3*2=6\t3*3=9\t\r\n" +
                "4*1=4\t4*2=8\t4*3=12\t4*4=16\t\r\n" +
                "5*1=5\t5*2=10\t5*3=15\t5*4=20\t5*5=25\t\r\n" +
                "6*1=6\t6*2=12\t6*3=18\t6*4=24\t6*5=30\t6*6=36\t\r\n" +
                "7*1=7\t7*2=14\t7*3=21\t7*4=28\t7*5=35\t7*6=42\t7*7=49\t\r\n" +
                "8*1=8\t8*2=16\t8*3=24\t8*4=32\t8*5=40\t8*6=48\t8*7=56\t8*8=64\t\r\n" +
                "9*1=9\t9*2=18\t9*3=27\t9*4=36\t9*5=45\t9*6=54\t9*7=63\t9*8=72\t9*9=81\t\r\n";
        mockMvc.perform(get("/api/tables/multiply"))
                .andExpect(content().string(actually));

    }

    @Test
    void should_return_mul_table_with_start_and_end() throws Exception {
        String actually = "3*3=9\t\r\n" +
                "4*3=12\t4*4=16\t\r\n" +
                "5*3=15\t5*4=20\t5*5=25\t\r\n";
        mockMvc.perform(get("/api/tables/multiply?start=3&end=5"))
                .andExpect(status().isOk())
                .andExpect(content().string(actually));
    }

    @Test
    void should_return_400_status_with_error_start_and_end() throws Exception {
        mockMvc.perform(get("/api/tables/multiply?start=3&end=10"))
                .andExpect(status().is(400));
        mockMvc.perform(get("/api/tables/multiply?start=6&end=5"))
                .andExpect(status().is(400));
    }

    @Test
    void should_return_mul_table_format_to_json() throws Exception {
        String expectedContent = "[" +
                "[\"4*4=16\"]," +
                "[\"5*4=20\",\"5*5=25\"]," +
                "[\"6*4=24\",\"6*5=30\",\"6*6=36\"]" +
                "]";
        mockMvc.perform(get("/api/tables/multiply/?start=4&end=6").accept(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    void should_return_mul_table_format_to_html() throws Exception {
        String expectedContent = "<ol>\n" +
                "  <li>\n" +
                "    <ol>\n" +
                "      <li>4*4=16</li>\n" +
                "    </ol>\n" +
                "  <li>\n" +
                "  <li>\n" +
                "    <ol>\n" +
                "      <li>5*4=20</li>\n" +
                "      <li>5*5=25</li>\n" +
                "    </ol>\n" +
                "  <li>\n" +
                "  <li>\n" +
                "    <ol>\n" +
                "      <li>6*4=24</li>\n" +
                "      <li>6*5=30</li>\n" +
                "      <li>6*6=36</li>\n" +
                "    </ol>\n" +
                "  <li>\n" +
                "</ol>\n";
        mockMvc.perform(get("/api/tables/multiply/?start=4&end=6").accept(TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }
}
