package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.http.MediaType.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class CheckAnswerControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Test
    void should_return_correct_json() throws Exception {
        mockMvc.perform(post("/api/check")
                .contentType(APPLICATION_JSON)
                .content("{" +
                        "  \"operandLeft\": 3," +
                        "  \"operandRight\": 5," +
                        "  \"operation\": \"+\"," +
                        "  \"expectedResult\": 8" +
                        "}"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("correct").value(true));
        mockMvc.perform(post("/api/check")
                .contentType(APPLICATION_JSON)
                .content("{" +
                        "  \"operandLeft\": 3," +
                        "  \"operandRight\": 5," +
                        "  \"operation\": \"*\"," +
                        "  \"expectedResult\": 15" +
                        "}"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("correct").value(true));


    }
    @Test
    void should_return_400_status_without_operation() throws Exception {
        mockMvc.perform(post("/api/check")
                .contentType(APPLICATION_JSON)
                .content("{" +
                        "  \"operandLeft\": 3," +
                        "  \"operandRight\": 5," +
                        "  \"expectedResult\": 8" +
                        "}"))
                .andExpect(status().is(400));

    }

    @Test
    void should_return_400_status_without_valid_type() throws Exception {
        mockMvc.perform(post("/api/check")
                .contentType(APPLICATION_JSON)
                .content("{" +
                        "  \"operandLeft\": \"a\"," +
                        "  \"operandRight\": 5," +
                        "  \"operation\": \"+\"," +
                        "  \"expectedResult\": 8" +
                        "}"))
                .andExpect(status().is(400));

        mockMvc.perform(post("/api/check")
                .contentType(APPLICATION_JSON)
                .content("{" +
                        "  \"operandLeft\": \"a\"," +
                        "  \"operandRight\": 5," +
                        "  \"operation\": \"*\"," +
                        "  \"expectedResult\": 15" +
                        "}"))
                .andExpect(status().is(400));

    }

    @Test
    void should_return_correct_json_with_check_type() throws Exception {
        mockMvc.perform(post("/api/check")
                .contentType(APPLICATION_JSON)
                .content("{" +
                        "  \"operandLeft\": 3," +
                        "  \"operandRight\": 5," +
                        "  \"operation\": \"+\"," +
                        "  \"expectedResult\": 8," +
                        "  \"checkType\": \"=\"" +
                        "}"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("correct").value(true));
    }
}