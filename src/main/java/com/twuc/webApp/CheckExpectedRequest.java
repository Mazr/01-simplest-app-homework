package com.twuc.webApp;

public class CheckExpectedRequest {
    private Boolean correct;

    public CheckExpectedRequest() {
    }

    CheckExpectedRequest(Boolean correct) {
        this.correct = correct;
    }

    public Boolean getCorrect() {
        return correct;
    }
}
