package com.twuc.webApp;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.beans.factory.annotation.Value;

import javax.validation.constraints.NotNull;

import static com.twuc.webApp.MyOperator.*;

public class RequestExpression {
    private String checkType = EQUAL ;
    @NotNull
    private Integer operandLeft;
    @NotNull
    private Integer operandRight;
    @NotNull
    private String operation;
    @NotNull
    private Integer expectedResult;

    private Boolean correct;

    public RequestExpression() {
    }

    public Integer getOperandLeft() {
        return operandLeft;
    }

    public Integer getOperandRight() {
        return operandRight;
    }

    public String getOperation() {
        return operation;
    }

    public Integer getExpectedResult() {
        return expectedResult;
    }

    public String getCheckType() {
        return checkType;
    }

    public CheckExpectedRequest getCheckExpectedRequest() {

        checkRequest();
        return new CheckExpectedRequest(correct);
    }

    private void checkRequest() {
        switch (operation) {
            case PLUS:
                checkAddWithType();
                break;
            case MULTIPLY:
                checkMulWithType();
                break;
            default:
                correct = null;
        }
    }

    private void checkAddWithType() {
        switch (checkType) {
            case GREATER:
                correct = ((operandLeft + operandRight) > expectedResult);
                break;
            case LESS:
                correct = ((operandLeft + operandRight) < expectedResult);
                break;
            default: correct = ((operandLeft + operandRight) == expectedResult);
        }
    }

    private void checkMulWithType() {
        switch (checkType) {
            case GREATER:
                correct = ((operandLeft * operandRight) > expectedResult);
                break;
            case LESS:
                correct = ((operandLeft * operandRight) < expectedResult);
                break;
            default:
                correct = ((operandLeft * operandRight) == expectedResult);
        }
    }
}
