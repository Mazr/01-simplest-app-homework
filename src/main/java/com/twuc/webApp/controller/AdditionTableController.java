package com.twuc.webApp.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.Arithmetic;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.stream.IntStream;

import static org.springframework.http.MediaType.*;

@RestController
@Validated
@RequestMapping("/api/tables")
public class AdditionTableController {
    @ExceptionHandler
    public ResponseEntity exceptionHandler(RuntimeException e) {
        return ResponseEntity.status(400).build();
    }

    @GetMapping("/plus")
    String getAddTableWithHtml(
            @RequestHeader(value = "Accept",defaultValue = "???") String acceptType,
            @RequestParam(defaultValue = "1") @Min(1) int start,
            @RequestParam(defaultValue = "9") @Max(9) int end)
            throws JsonProcessingException {
        if (start > end)
            throw new RuntimeException();

        if (acceptType.equals("text/html"))
            return creatAddTableWithHtml(start, end);
        else if (acceptType.equals("application/json"))
            return creatAddTableWithJson(start, end);

        return creatAddTable(start, end);
    }

    private String creatAddTable(int start, int end) {
        String addTable = "";
        for (int a = start; a <= end; a++) {
            for (int b = start; b <= a; b++) {
                addTable = String.format("%s%s%s", addTable, Arithmetic.Addition(a, b).getExpression(), '\t');
            }
            addTable += "\r\n";
        }
        return addTable;
    }


    private String creatAddTableWithJson(int start, int end) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(
                IntStream.rangeClosed(start, end)
                        .mapToObj(i -> IntStream.rangeClosed(start, i)
                                .mapToObj(j -> Arithmetic.Addition(i, j).getExpression())
                                .toArray())
                        .toArray(Object[][]::new)
        );
    }


    private String creatAddTableWithHtml(int start, int end) throws JsonProcessingException {
        String olLable = "<ol>\n";
        String addTable = olLable;
        String endLineLable = "    </ol>\n  <li>\n";
        String startLineLabel = "  <li>\n    <ol>\n";
        for (int a = start; a <= end; a++) {
            addTable += startLineLabel;
            for (int b = start; b <= a; b++) {
                addTable = String.format("%s%s%s%s", addTable, "      <li>", Arithmetic.Addition(a, b).getExpression(), "</li>\n");
            }
            addTable += endLineLable;
        }
        addTable += "</ol>\n";
        return addTable;
    }


}
