package com.twuc.webApp.controller;

import com.twuc.webApp.RequestExpression;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class CheckAnswerController {
    @PostMapping("/api/check")
    ResponseEntity checkAnswer(@Valid @RequestBody RequestExpression requestExpression ){
        return ResponseEntity.status(200).body(requestExpression.getCheckExpectedRequest());
    }
}
