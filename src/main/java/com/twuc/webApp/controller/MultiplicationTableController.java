package com.twuc.webApp.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.Arithmetic;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.stream.IntStream;

@RestController
@Validated
@RequestMapping("/api/tables")
public class MultiplicationTableController {
    @ExceptionHandler
    public ResponseEntity exceptionHandler(RuntimeException e) {
        return ResponseEntity.status(400).build();
    }

    @GetMapping("/multiply")
    String getMulTable(
            @RequestHeader(value = "Accept",defaultValue = "text/plain") String acceptType,
            @RequestParam(defaultValue = "1") @Min(1) int start,
            @RequestParam(defaultValue = "9") @Max(9) int end) throws JsonProcessingException {
        if (start > end)
            throw new RuntimeException();
        if (acceptType.equals("text/html"))
            return creatMulTableWithHtml(start, end);
        else if (acceptType.equals("application/json"))
            return creatMulTableWithJson(start, end);
        return creatMulTable(start,end);
    }

    private String creatMulTable(int start, int end) {
        String mulTable = "";
        for (int a = start; a <= end; a++) {
            for (int b = start; b <= a; b++) {
                mulTable = String.format("%s%s%s", mulTable, Arithmetic.Multiplication(a,b).getExpression(),'\t');
            }
            mulTable += "\r\n";
        }
        return mulTable;
    }


    private String creatMulTableWithJson(int start, int end) throws JsonProcessingException {
        ObjectMapper mapper =new ObjectMapper();
        return mapper.writeValueAsString(
                IntStream.rangeClosed(start,end)
                        .mapToObj(i -> IntStream.rangeClosed(start,i)
                                .mapToObj(j -> Arithmetic.Multiplication(i, j).getExpression())
                                .toArray())
                        .toArray(Object[][]::new)
        );
    }


    private String creatMulTableWithHtml(int start, int end) throws JsonProcessingException {
        String olLable = "<ol>\n";
        String mulTable = olLable;
        String endLineLable = "    </ol>\n  <li>\n";
        String startLineLabel = "  <li>\n    <ol>\n";
        for (int a = start; a <= end; a++) {
            mulTable += startLineLabel;
            for (int b = start; b <= a; b++) {
                mulTable = String.format("%s%s%s%s", mulTable, "      <li>",Arithmetic.Multiplication(a,b).getExpression(),"</li>\n");
            }
            mulTable += endLineLable;
        }
        mulTable += "</ol>\n";
        return mulTable;
    }
}
