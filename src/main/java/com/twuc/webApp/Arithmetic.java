package com.twuc.webApp;


public class Arithmetic {
    public String getExpression() {
        return expression;
    }

    private String expression;

    public static Arithmetic Addition(int a, int b) {
        return new Arithmetic(MyOperator.PLUS, a+b, a, b);
    }

    public static Arithmetic Multiplication(int a, int b) {
        return new Arithmetic(MyOperator.MULTIPLY, a*b, a, b);
    }

    private Arithmetic(String operator, int result, int a, int b) {
        this.expression = a +
                operator +
                b +
                MyOperator.EQUAL +
                result;
    }
}
